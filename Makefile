CC = clang
LD = ld
BIN = ./bin/main
MAIN = src/main.c
FORMAT = clang-format

# ADD NEW SOURCE FILES THESE
CFILES =
HFILES =
OBJFILES =

FMTFILES = src/main.c.fmt

CFLAGS = -std=c99 -Wall -Wextra -pedantic


all: clean $(BIN)

$(BIN): $(OBJFILES)
	$(CC) $(CFLAGS) -o $@ $(MAIN) $^

obj_files/%.o:src/%.c
	$(CC) $(CFLAGS) -c -o $@ $^

fmt: $(FMTFILES)

%.c.fmt: %.c 
	$(FORMAT) --style=GNU $< > $@
	mv $@ $<

%.h.fmt: %.h 
	$(FORMAT) --style=GNU $< > $@
	mv $@ $<

clean:
	rm -rf $(OBJFILES) $(BIN)

run: all
	$(BIN)
