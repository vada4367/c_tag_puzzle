#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void
print_puzzle (int puzzle[4][4])
{
  for (int i = 0; i < 4; ++i)
    {
      for (int j = 0; j < 4; ++j)
        {
          if (puzzle[i][j] == 0)
            printf ("%3c ", ' ');
          else
            printf ("%3d ", puzzle[i][j]);
        }

      printf ("\n");
    }
}

void
move_puzzle (int puzzle[4][4], char input[10])
{
  int pos = strtol (input, NULL, 10);
  pos %= 100;
  int y = pos / 10 - 1;
  int x = pos % 10 - 1;
  if (x < 0 || x > 3 || y < 0 || y > 3)
    return;
  if (x != 0 && puzzle[x - 1][y] == 0)
    {
      puzzle[x - 1][y] = puzzle[x][y];
      puzzle[x][y] = 0;
      return;
    }
  if (x != 3 && puzzle[x + 1][y] == 0)
    {
      puzzle[x + 1][y] = puzzle[x][y];
      puzzle[x][y] = 0;
      return;
    }
  if (y != 0 && puzzle[x][y - 1] == 0)
    {
      puzzle[x][y - 1] = puzzle[x][y];
      puzzle[x][y] = 0;
      return;
    }
  if (y != 3 && puzzle[x][y + 1] == 0)
    {
      puzzle[x][y + 1] = puzzle[x][y];
      puzzle[x][y] = 0;
      return;
    }
}

int
main (void)
{
  srand (time (NULL));

  int puzzle[4][4] = { 0 };
  for (int i = 1; i < 16; ++i)
    {
      int rx = 0, ry = 0;
      for (; puzzle[rx][ry] != 0; rx = rand () % 4, ry = rand () % 4)
        ;
      puzzle[rx][ry] = i;
    }

  char string[10];

  for (;;)
    {
      print_puzzle (puzzle);
      fgets (string, 10, stdin);
      move_puzzle (puzzle, string);
    }

  return 0;
}
